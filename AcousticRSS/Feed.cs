﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Xml;
using System.Threading;

namespace AcousticRSS {
    public partial class Feed : UserControl {
        // Class variables
        XmlDocument Xml = new XmlDocument();
        char[] seperators = {' ', ','};
        List<int> ignore = new List<int>();
        int checks = 0;

        // Event handlers
        private void check_btn_Click(object sender, EventArgs e) {
            Check();
        }

        // Functions
        public void Check() {
            if(active_box.Checked) {
                if(checks == 10) {
                    checks = 0;
                    ignore.RemoveAll(item => item > -1);
                }

                using(XmlReader reader = XmlReader.Create(url_text.Text)) {
                    Xml.Load(reader);
                }

                string[] flags = flags_text.Text.Split(seperators);

                string data = Xml.DocumentElement.InnerText.ToLower();
                string found = "";
                for(int i = 0; i < flags.Length; i++) {
                    if(data.IndexOf(flags[i].ToLower()) != -1
                        && ignore.IndexOf(data.IndexOf(flags[i].ToLower())) == -1
                        && flags[i].ToLower() != "") {
                        found += flags[i].ToLower() + ", ";
                        ignore.Add(data.IndexOf(flags[i].ToLower()));
                    }
                }

                if(found != "") {
                    MessageBox.Show(found + "found on feed: " + this.name_text.Text,
                                    "Flag(s) Found",
                                    MessageBoxButtons.OK);
                }
            }
            checks++;
        }

        // Constructor
        public Feed() {
            InitializeComponent();
        }
    }
}
