﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;
using System.Xml;
using System.IO;

namespace AcousticRSS {
    public partial class AcousticRSS : Form {
        // Class variables
        List<Feed> Feeds = new List<Feed>();
        Thread CheckThread;
        XmlDocument Xml;

        // Event handlers
        private void add_btn_Click(object sender, EventArgs e) {
            AddFeed();
        }
        void Delete(object sender, EventArgs e) {
            Button btn = (Button)sender;
            Feeds.RemoveAt(Feeds.IndexOf((Feed)btn.Parent));
            btn.Parent.Dispose();

            for(int i = 0; i < Feeds.Count; i++) {
                if(i == 0) {
                    Feeds[i].Location = new Point(10, 10 - scrollbar.Value);
                } else {
                    Feeds[i].Location = new Point(10, (Feeds[i - 1].Location.Y + Feeds[i - 1].Height + 10) - scrollbar.Value);
                }
            }

            UpdateScrollBar();
        }
        private void feeds_pan_Resize(object sender, EventArgs e) {
            UpdateScrollBar();
        }
        private void delay_select_ValueChanged(object sender, EventArgs e) {
            if(delay_select.Value <= 0) delay_select.Value = 1;
        }
        private void AcousticRSS_FormClosing(object sender, FormClosingEventArgs e) {
            CheckThread.Abort();
        }
        private void scrollbar_Scroll(object sender, ScrollEventArgs e) {
            for(int i = 0; i < Feeds.Count; i++) {
                int oldy = Feeds[i].Location.Y;
                Feeds[i].Location = new Point(10, oldy + (e.OldValue - e.NewValue));
            }
        }
        private void save_btn_Click(object sender, EventArgs e) {
            using(StreamWriter writer = new StreamWriter("Feeds.xml")) {
                XmlNode root = Xml.SelectSingleNode("AcousticRSS");
                root.RemoveAll();

                foreach(Feed feed in Feeds) {
                    XmlNode feedNode = Xml.CreateNode("element", "Feed", "");

                    XmlNode name = Xml.CreateNode("element", "Name", "");
                    name.InnerText = feed.name_text.Text;

                    XmlNode url = Xml.CreateNode("element", "URL", "");
                    url.InnerText = feed.url_text.Text;

                    XmlNode flags = Xml.CreateNode("element", "Flags", "");
                    flags.InnerText = feed.flags_text.Text;

                    feedNode.AppendChild(name);
                    feedNode.AppendChild(url);
                    feedNode.AppendChild(flags);

                    root.AppendChild(feedNode);
                }

                Xml.Save(writer);
            }
        }

        // Functions
        void UpdateScrollBar() {
            if(Feeds.Count > 0) {
                int dif = ((Feeds[Feeds.Count - 1].Location.Y + Feeds[Feeds.Count - 1].Height) - feeds_pan.Height);
                if(dif > -1) {
                    scrollbar.Maximum = dif;
                } else {
                    scrollbar.Maximum = 0;
                }
            }
        }
        void Check() {
            Thread.Sleep((int)delay_select.Value * 60000);
            for(int i = 0; i < Feeds.Count; i++) {
                Feeds[i].Check();
            }
        }
        void AddFeed(string name = "/r/Free Feed", string url = "http://reddit.com/r/FREE/.rss", string flags = "reddit") {
            Feed feed = new Feed();

            if(Feeds.Count >= 1) {
                feed.Location = new Point(10, Feeds[Feeds.Count - 1].Location.Y + Feeds[Feeds.Count - 1].Height + 10);
            } else {
                feed.Location = new Point(10, 10);
            }
            feed.delete_btn.Click += new EventHandler(Delete);

            feed.name_text.Text = name;
            feed.url_text.Text = url;
            feed.flags_text.Text = flags;

            Feeds.Add(feed);
            this.feeds_pan.Controls.Add(feed);

            UpdateScrollBar();
        }

        // Constructor
        public AcousticRSS() {
            InitializeComponent();

            Xml = new XmlDocument();

            try {
                using(StreamReader reader = new StreamReader("Feeds.xml")) {
                    Xml.Load(reader);

                    XmlNode root = Xml.GetElementsByTagName("AcousticRSS")[0];
                    XmlNodeList feeds = root.SelectNodes("Feed");
                    foreach(XmlNode feed in feeds) {
                        AddFeed(feed.SelectSingleNode("Name").InnerText,
                                feed.SelectSingleNode("URL").InnerText,
                                feed.SelectSingleNode("Flags").InnerText);
                    }
                }
            } catch(Exception e) {
                using(StreamWriter writer = new StreamWriter("Feeds.xml")) {
                    Xml.AppendChild(Xml.CreateNode("element", "AcousticRSS", ""));
                    Xml.Save(writer);
                }
            }

            CheckThread = new Thread(Check);
            CheckThread.Start();
        }
    }
}
