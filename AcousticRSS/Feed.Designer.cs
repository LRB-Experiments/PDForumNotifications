﻿namespace AcousticRSS {
    partial class Feed {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.active_box = new System.Windows.Forms.CheckBox();
            this.check_btn = new System.Windows.Forms.Button();
            this.name_text = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.url_text = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.flags_text = new System.Windows.Forms.TextBox();
            this.delete_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // active_box
            // 
            this.active_box.AutoSize = true;
            this.active_box.Checked = true;
            this.active_box.CheckState = System.Windows.Forms.CheckState.Checked;
            this.active_box.Location = new System.Drawing.Point(309, 4);
            this.active_box.Name = "active_box";
            this.active_box.Size = new System.Drawing.Size(56, 17);
            this.active_box.TabIndex = 2;
            this.active_box.Text = "Active";
            this.active_box.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.active_box.UseVisualStyleBackColor = true;
            // 
            // check_btn
            // 
            this.check_btn.Location = new System.Drawing.Point(126, 89);
            this.check_btn.Name = "check_btn";
            this.check_btn.Size = new System.Drawing.Size(99, 23);
            this.check_btn.TabIndex = 3;
            this.check_btn.Text = "Manual Check";
            this.check_btn.UseVisualStyleBackColor = true;
            this.check_btn.Click += new System.EventHandler(this.check_btn_Click);
            // 
            // name_text
            // 
            this.name_text.Location = new System.Drawing.Point(47, 2);
            this.name_text.Name = "name_text";
            this.name_text.Size = new System.Drawing.Size(256, 20);
            this.name_text.TabIndex = 6;
            this.name_text.Text = "/r/FREE Feed";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "URL:";
            // 
            // url_text
            // 
            this.url_text.Location = new System.Drawing.Point(47, 26);
            this.url_text.Name = "url_text";
            this.url_text.Size = new System.Drawing.Size(256, 20);
            this.url_text.TabIndex = 9;
            this.url_text.Text = "http://reddit.com/r/FREE/.rss";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Flags (seperated by commas):";
            // 
            // flags_text
            // 
            this.flags_text.Location = new System.Drawing.Point(159, 63);
            this.flags_text.Name = "flags_text";
            this.flags_text.Size = new System.Drawing.Size(144, 20);
            this.flags_text.TabIndex = 11;
            this.flags_text.Text = "reddit";
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(228, 89);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(75, 23);
            this.delete_btn.TabIndex = 12;
            this.delete_btn.Text = "Delete";
            this.delete_btn.UseVisualStyleBackColor = true;
            // 
            // Feed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.delete_btn);
            this.Controls.Add(this.flags_text);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.url_text);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.name_text);
            this.Controls.Add(this.check_btn);
            this.Controls.Add(this.active_box);
            this.Name = "Feed";
            this.Size = new System.Drawing.Size(368, 122);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox active_box;
        private System.Windows.Forms.Button check_btn;
        public System.Windows.Forms.TextBox name_text;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox url_text;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox flags_text;
        public System.Windows.Forms.Button delete_btn;
    }
}
