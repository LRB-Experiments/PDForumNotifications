# PDForumNotifications
Checks the Project Dollhouse forums for new content; uses the RSS feed.  Based off of SeymourApps' AcousticRSS.

# Credit
Icon by Icons8 (www.icons8.com)

### Original program requested by www.reddit.com/u/acustic
